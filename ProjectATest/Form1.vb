﻿Imports System.Collections.ObjectModel
Imports System.Data.SqlClient
Imports System.Runtime.Remoting

Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        BindGrid()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        UpdateRow()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        CreateNew()
    End Sub

    Private Sub UpdateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UpdateNew()
    End Sub

    Dim connectionString = "Data Source=localhost\SQLEXPRESS;Database=projectA;Trusted_Connection=True;"
    Dim da As SqlDataAdapter
    Dim cmd As SqlCommand
    Dim cmdBuilder As SqlCommandBuilder
    Dim ds As DataSet
    Dim dss As DataSet
    Dim da2 As SqlDataAdapter
    Dim ds2 As DataSet
    Dim dss2 As DataSet
    Sub BindGrid()
        Try
            Dim conn As System.Data.SqlClient.SqlConnection = New SqlClient.SqlConnection(connectionString)
            cmd = New SqlCommand
            cmd.CommandText = "Select * from Tests"
            da = New SqlDataAdapter(cmd.CommandText, conn)
            ds = New DataSet()
            da.Fill(ds, "Tests")
            If (ds.Tables(0).Rows.Count <= 2) Then
                For i As Integer = 1 To 3
                    Dim row As DataRow
                    row = ds.Tables(0).NewRow
                    ds.Tables(0).Rows.Add(row)
                Next
            End If

            DataGridView1.DataSource = ds.Tables(0)
            DataGridView1.Columns("Id").Visible = False

            For Each row As DataGridViewRow In DataGridView1.Rows
                For i = 0 To row.Cells.Count - 1
                    row.Cells(i).ToolTipText = String.Concat("Column ", row.Index, " ", i)
                Next
            Next
            conn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Sub UpdateRow()
        Try
            cmdBuilder = New SqlCommandBuilder(da)
            dss = ds.GetChanges()
            If dss IsNot Nothing Then
                da.Update(dss, "Tests")
            End If
            MessageBox.Show("Changes Done")
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub

    Sub CreateNew()
        Dim dgv As New DataGridView
        Dim col As Integer = CInt(InputBox("How many columns", "Columns", "3"))
        Dim row As Integer = CInt(InputBox("How many rows", "Rows", "3"))
        Dim table2 As New DataTable
        table2.Columns.AddRange(New DataColumn() {New DataColumn("Id", GetType(System.Int32))})

        Dim colStr = ""
        For c As Integer = 0 To col - 1
            table2.Columns.AddRange(New DataColumn() {New DataColumn("Col" + c.ToString, GetType(System.String))})
            colStr += "Col" + c.ToString + " VARCHAR(20), "
        Next
        For r As Integer = 0 To row - 1
            table2.Rows.Add(r)
        Next
        dgv.DataSource = table2
        Me.Controls.Add(dgv)
        dgv.Columns("Id").Visible = False
        dgv.Location = New Point(45, 350)
        dgv.Size = New Size(450, 150)

        Dim updateButton As New Button
        updateButton.Location = New Point(400, 326)
        updateButton.Height = 21
        updateButton.Width = 107
        updateButton.Text = "Update New"
        updateButton.Name = "UpdateButton"

        AddHandler updateButton.Click, AddressOf UpdateButton_Click
        Controls.Add(updateButton)

        Dim conn As System.Data.SqlClient.SqlConnection = New SqlClient.SqlConnection(connectionString)
        Dim cmd As New SqlCommand
        cmd.CommandText = "CREATE TABLE Tests2 " &
                          "(" &
                          "Id int NOT NULL, " &
                          colStr &
                          "PRIMARY KEY(Id)" &
                          ")"
        cmd.Connection = conn
        conn.Open()
        cmd.ExecuteNonQuery()
        cmd.Dispose()
        conn.Close()

        cmd.CommandText = "Select * from Tests2"
        da2 = New SqlDataAdapter(cmd.CommandText, conn)
        ds2 = New DataSet()
        da2.Fill(ds2, "Tests2")
        ds2.Tables.Add(table2)
        dgv.DataSource = ds2.Tables(1)
    End Sub

    Sub UpdateNew()
        Try
            cmdBuilder = New SqlCommandBuilder(da2)
            dss2 = ds2.GetChanges()
            If dss2 IsNot Nothing Then
                da2.Update(dss2, "Table1")
            End If
            MessageBox.Show("Changes Done")
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
End Class